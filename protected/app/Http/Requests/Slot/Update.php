<?php

namespace App\Http\Requests\Slot;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'slot_name' => 'sometimes|required|unique:slot,slot_name,'.$request->id_slot.',id_slot',
            'capacity'  => 'sometimes|integer'
        ];
    }

    public function messages() {
        return [
            'slot_name.unique' => 'Nama slot sudah terdaftar',
            'capacity.integer' => 'Kapasitas harus berupa angka',
        ];
    }
}