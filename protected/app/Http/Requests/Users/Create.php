<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip'   => 'sometimes|required|unique:users,nip',
            'email' => 'sometimes|email'
        ];
    }

    public function messages() {
        return [
            'nip.unique'  => 'NIP sudah terdaftar',
            'email.email' => 'Alamat email tidak valid',
        ];
    }
}