<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_slot
 * @property string $slot_name
 * @property int $capacity
 * @property string $created_at
 * @property string $updated_at
 * @property SlotKayu[] $slotKayus
 */


use App\Http\Models\SlotKayu;
use DB;

class Slot extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'slot';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_slot';

    /**
     * @var array
     */
    protected $fillable = ['slot_name', 'capacity', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slotKayus()
    {
        return $this->hasMany('App\Http\Models\SlotKayu', 'id_slot', 'id_slot');
    }

    /* SAVE */
    static function saveData($post) {
        $save = Self::create($post);

        return $save;
    }

    /* LIST */
    static function listSlot($post=[]) {
        $slot = Self::select('*');

        if (isset($post['id_slot'])) {
            $slot->where('id_slot', $post['id_slot']);
        }

        $slot = $slot->get()->toArray();

        return $slot;
    }

    /* UPDATE */
    static function updateData($data) {
        $slot = Self::where('id_slot', $data['id_slot'])->update($data);

        return $slot;
    }

    /* DELETE */
    static function deleteSlot($id_slot) {
        if (static::checkDeleteData($id_slot)) {
            // DELETE
            $delete = Self::where('id_slot', $id_slot)->delete();

            return $delete;
        }
        else {
            return false;
        }
    }

    /* CEK DELETE */
    static function checkDeleteData($id) {
        $cek = SlotKayu::where('id_slot', $id)->first();

        if (empty($cek)) {
            return true;
        }
        else {
            return false;
        }
    }
}
