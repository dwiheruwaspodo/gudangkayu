<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_kayu
 * @property string $code
 * @property int $length
 * @property int $width
 * @property string $created_at
 * @property string $updated_at
 * @property SlotKayu[] $slotKayus
 */
class Kayu extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'kayu';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_kayu';

    /**
     * @var array
     */
    protected $fillable = ['code', 'length', 'width', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slotKayus()
    {
        return $this->hasMany('App\Http\Models\SlotKayu', 'id_kayu', 'id_kayu');
    }
}
