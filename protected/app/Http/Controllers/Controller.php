<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function redirect($save, $messagesSuccess, $next=null) {
        if ($save) {
            if (is_null($next)) {
                return back()->with('success', [$messagesSuccess]);
            }
            else {
                return redirect($next)->with('success', [$messagesSuccess]);
            }
        }
        else {
            $e = ['e' => 'Terjadi kesalahan. Silakan coba lagi.'];
            return back()->witherrors($e)->withInput();
        }
    }
}
