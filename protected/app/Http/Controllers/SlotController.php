<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Slot;
use Hash;

use App\Http\Requests\Slot\Create;

class SlotController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
    }

    /* CREATE */
    function create(Create $request) {
        $post = $request->except('_token');

        if (empty($post)) {
            $data = [
                'title'    => 'Tambah Slot',
                'menu'     => 'slot',
                'sub_menu' => 'slot tambah'
            ];

            return view('content.slot.create', $data);
        }
        else {
            $save = Slot::saveData($post);

            return parent::redirect($save, 'Slot baru berhasil ditambahkan.');
        }
    }

    /* LIST */
    function index(Request $request) {
        $data = [
            'title'    => 'List Slot',
            'menu'     => 'slot',
            'sub_menu' => 'slot list'
        ];

        $data['slot'] = Slot::listSlot([]);

        return view('content.slot.list', $data);
    }

    /* UPDATE */
    function update(Request $request, $id, $slug) {

        $post = $request->except('_token');

        if (empty($post)) {
            $data = [
                'title'    => 'Update Slot',
                'menu'     => 'slot',
                'sub_menu' => 'slot list'
            ];

            $data['slot'] = Slot::listSlot(['id_slot' => $id]);

            if (empty($data['slot'])) {
                return back()->withErrors(['Data tidak ditemukan.']);
            }

            return view('content.slot.update', $data);
        }
        else {
            $save = Slot::updateData($post);

            return parent::redirect($save, 'Data Slot berhasil diupdate.');   
        }
    }

    /* DELETE */
    function delete(Request $request) {
        $post = $request->except('_token');

        $delete = Slot::deleteSlot($post['id_slot']);

        if (!$delete) {
            return back()->withErrors(['Gagal menghapus data. Slot terkait transaksi lain.']);
        }

        return parent::redirect($delete, 'Data Slot berhasil dihapus.');   
    }
}
