<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\User;
use Hash;
use Auth;

use App\Http\Requests\Users\Create;
use App\Http\Requests\Users\ChangePassword;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
    }

    /* CREATE */
    function create(Create $request) {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		$data = [
				'title'    => 'Tambah User',
				'menu'     => 'user',
				'sub_menu' => 'user tambah'
    		];

    		return view('content.user.create', $data);
    	}
    	else {
    		// SAVE DATA
    		$save = User::saveData($post);

    		return parent::redirect($save, 'User baru berhasil ditambahkan.');
    	}
    }

    /* LIST */
    function index(Request $request) {
    	$data = [
			'title'    => 'List User',
			'menu'     => 'user',
			'sub_menu' => 'user list'
		];

		$data['user'] = User::listUser([]);

		return view('content.user.list', $data);

    }

    /* UPDATE */
    function update(Request $request, $nip) {

    	$post = $request->except('_token');

    	if (empty($post)) {
	    	$data = [
				'title'    => 'Update User',
				'menu'     => 'user',
				'sub_menu' => 'user list'
			];

			$data['user'] = User::listUser(['nip' => $nip]);

			return view('content.user.update', $data);	
    	}
    	else {
    		// SAVE DATA
    		$save = User::updateData($post);

    		return parent::redirect($save, 'Data user berhasil diupdate.');
    	}
    }

    /* GANTI PASSWORD */
    function gantiPassword(ChangePassword $request) {
        $post = $request->except('_token');

        if (empty($post)) {
            $data = [
                'title'    => 'Ganti Password',
                'menu'     => 'user',
                'sub_menu' => 'user list'
            ];

            return view('content.user.change_password', $data);
        }
        else {
            $user = [
                'id'       => Auth::user()->id,
                'password' => $post['password_now']
            ];


            if (Auth::attempt($user)) {
                $dataUpdate = [
                    'id'       => Auth::user()->id,
                    'password' => Hash::make($post['password'])
                ];

                $update = User::updateData($dataUpdate);

                if ($update) {
                    Auth::logout();
                    return redirect('login')->with('success', ['Silakan login dengan dengan password baru.']);
                }
            }
            else {
                return back()->withErrors(['Password sekarang tidak cocok.']);
            }

        }
    }
}
