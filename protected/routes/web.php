<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['web', 'auth']], function()
{
	/* GANTI PASSWORD */
	Route::any('user/ganti-password', 'UserController@gantiPassword');
	
	/* USER */
	Route::group(['prefix' => 'user', 'middleware' => ['feature_control:admin|officer']], function()
	{
	    Route::get('/', 'UserController@index');
	    Route::any('tambah', 'UserController@create');
	    Route::any('update/{id}', 'UserController@update');
	    Route::post('hapus', 'UserController@delete');
	});

	/* SLOT */
	Route::group(['prefix' => 'slot', 'middleware' => ['feature_control:admin|officer']], function()
	{
	    Route::get('/', 'SlotController@index');
	    Route::any('tambah', 'SlotController@create');
	    Route::any('update/{id}/{slug}', 'SlotController@update');
	    Route::post('hapus', 'SlotController@delete');
	});

	/* IN */
	Route::group(['prefix' => 'in', 'middleware' => ['feature_control:admin|officer']], function()
	{
	    Route::any('/', 'InController@index');
	    Route::any('tambah', 'InController@create');
	});
});