<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogOutDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_out_detail', function (Blueprint $table) {
            $table->increments('id_log_out_detail');

            // logout
            $table->unsignedInteger('id_log_out');
            $table->foreign('id_log_out')->references('id_log_out')->on('log_out')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            // log kayu
            $table->unsignedInteger('id_slot_kayu');
            $table->foreign('id_slot_kayu')->references('id_slot_kayu')->on('slot_kayu')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('qty');
            $table->string('remark', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_out_detail');
    }
}
