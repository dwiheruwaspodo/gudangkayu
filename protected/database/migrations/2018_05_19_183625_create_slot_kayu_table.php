<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotKayuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slot_kayu', function (Blueprint $table) {
            $table->increments('id_slot_kayu');

            // slot
            $table->unsignedInteger('id_slot');
            $table->foreign('id_slot')->references('id_slot')->on('slot')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            // kayu
            $table->unsignedInteger('id_kayu');
            $table->foreign('id_kayu')->references('id_kayu')->on('kayu')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('qty');
            $table->boolean('flag')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slot_kayu');
    }
}
