<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_out', function (Blueprint $table) {
            $table->increments('id_log_out');
            $table->string('to', 60);
            $table->string('address', 255);
            $table->dateTime('delivery_date');
            $table->string('licence_plate', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_out');
    }
}
