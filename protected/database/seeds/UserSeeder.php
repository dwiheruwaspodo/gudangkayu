<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert([
    		[
				'nip'            => '123456789',
				'name'           => 'Admin',
				'phone'          => '0811223344',
				'email'          => 'admin@gudangkayu.com',
				'password'       => Hash::make('123123'),
				'level'          => 'admin',
				'remember_token' => Hash::make('123123'),
				'created_at'     => date('Y-m-d H:i:s'),
				'updated_at'     => date('Y-m-d H:i:s'),
    		]
        ]);
    }
}
