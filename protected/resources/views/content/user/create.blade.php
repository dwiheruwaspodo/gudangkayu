@extends('template.body')

@section('style')
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>User</li>
	    	<li class="active">Tambah</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
	      	<div class="box-body">
		        <div class="form-group">
		          	<label class="col-md-2 control-label">NIP <small style="color: red">*</small> </label> 
		          	<div class="col-md-10">
		            	<input type="text" class="form-control" name="nip" placeholder="NIP" required maxlength="100" value="{{ old('nip') }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Nama <small style="color: red">*</small> </label>
		          	<div class="col-md-10">
		            	<input type="text" class="form-control" name="name" placeholder="Nama" required maxlength="100" value="{{ old('name') }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Email</label>
		          	<div class="col-md-10">
		            	<input type="email" class="form-control" name="email" placeholder="Email" maxlength="100" value="{{ old('email') }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Phone</label>
		          	<div class="col-md-10">
		            <input type="text" class="form-control" name="phone" placeholder="Phone" maxlength="100" value="{{ old('phone') }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Level <small style="color: red">*</small> </label>
					<div class="col-md-2">
			          	<div class="checkbox">
	                       	<input type="radio" name="level" value="admin" required>
	                      	<label>
	                        Admin
	                      	</label>
	                    </div>
                    </div>
					<div class="col-md-2">
			          	<div class="checkbox">
	                       	<input type="radio" name="level" value="officer" required>
	                      	<label>
	                        Officer
	                      	</label>
	                    </div>
                    </div>
					<div class="col-md-2">
			          	<div class="checkbox">
	                       	<input type="radio" name="level" value="marketing" required>
	                      	<label>
	                        Marketing
	                      	</label>
	                    </div>
                    </div>
					<div class="col-md-2">
			          	<div class="checkbox">
	                       	<input type="radio" name="level" value="manager" required>
	                      	<label>
	                        Manager
	                      	</label>
	                    </div>
                    </div>
		        </div>
	      	</div>
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
				{{ csrf_field() }}	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();
	});
</script>
@endsection 