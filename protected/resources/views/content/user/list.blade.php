@extends('template.body')

@section('style')
	
	<link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css">') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>User</li>
	    	<li class="active">List</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <div class="box-body">
	      <table id="table01" class="table table-bordered table-striped">
	        <thead>
	        <tr>
	          	<th>NIK</th>
	          	<th>Nama</th>
	          	<th>Email</th>
	          	<th>Phone</th>
	          	<th>Level</th>
	          	<th>Status</th>
	          	<th>Action</th>
	        </tr>
	        </thead>
	        <tbody>
	        @if (!empty($user))
		        @foreach ($user as $key=>$val)
		        	<tr>
			          	<td>{{ $val['nip'] }}</td>
			          	<td>{{ $val['name'] }}</td>
			          	<td>{{ $val['email'] }}</td>
			          	<td>{{ $val['phone'] }}</td>
			          	<td>{{ ucwords($val['level']) }}</td>
			          	<td>@if (empty($val['deleted_at'])) Aktif @else Banned @endif</td>
			          	<td>
			          		<a href="{{ url('user/update', $val['nip']) }}" class="btn btn-success"><i class="fa fa-edit"></i> Update </a>
			          		
			          		<!-- banned -->
			          		@if (empty($val['deleted_at'])) 
			          			<button class="btn btn-danger delete" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id'] }}"> <i class="fa fa-ban"></i> </button>

			          			<form id="delete-{{ $val['id'] }}" action="{{ url('user/update', $val['nip']) }}" method="POST" style="display: none;">
			          			    {{ csrf_field() }}
			          			    <input type="hidden" name="deleted_at" value="{{ date('Y-m-d H:i:s') }}">
			          			    <input type="hidden" name="id" value="{{ $val['id'] }}">
			          			</form>
			          		@else
			          			<button class="btn btn-info delete" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id'] }}"> <i class="fa fa-check"></i> </button>

			          			<form id="delete-{{ $val['id'] }}" action="{{ url('user/update', $val['nip']) }}" method="POST" style="display: none;">
			          			    {{ csrf_field() }}
			          			    <input type="hidden" name="deleted_at" value="no">
			          			    <input type="hidden" name="id" value="{{ $val['id'] }}">
			          			</form>
			          		@endif

			          		
			          	</td>
			        </tr>
			    @endforeach
		    @endif
	        </tbody>
	      </table>
	    </div>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script src="{{ url('js/bootstrap-confirmation.js') }}"></script>
<script>
  $(function () {
    $('#table01').DataTable();
    
  });
</script>
<script type="text/javascript">
	$('[data-toggle=confirmation-popout]').confirmation({
	    rootSelector: '[data-toggle=confirmation-popout]',
	    container: 'body'
	});
</script>
<script type="text/javascript">
	$('#table01').on('click', '.delete', function() {
		$('#delete-'+$(this).data('id')).submit();
	});

	$('#table01').on('click', '.admin', function() {
		$('#admin-'+$(this).data('id')).submit();
	});
</script>

@endsection 