@extends('template.body')

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home active"></i> Home</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
  	<!-- Default box -->
	<div class="box">
	  	<div class="box-header with-border">
	  		@php
	  			$salam = "";

				if (strtotime(date('H:i')) >= strtotime('12:00') && strtotime(date('H:i')) <= strtotime('15:00')) {
					$salam = "Selamat Siang";
				}
				elseif (strtotime(date('H:i')) >= strtotime('15:00') && strtotime(date('H:i')) <= strtotime('17:00')) {
					$salam = "Selamat Sore";
				}
				elseif (strtotime(date('H:i')) >= strtotime('17:00') && strtotime(date('H:i')) <= strtotime('23:59')) {
					$salam = "Selamat Malam";
				}
				else {
					$salam = "Selamat Pagi";
				}
	  		@endphp
		  	<h3 class="box-title">{{ $salam }}, {{ Auth::user()->name }}!</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table class="table table-bordered">
		    
			@php
				$show = ['nip', 'name', 'phone', 'email', 'level'];	
			@endphp

		    @foreach ($user as $key=>$val)
		    <tr>

		    	@if (in_array($key, $show))
			    	<td><strong> {{ ucwords($key) }} </strong></td>
			    	<td> {{ $val }}</td>
		    	@endif
		    </tr>
		    @endforeach

		  </table>
		</div>
	    
	    <!-- /.box-body -->
	    <!-- <div class="box-footer">
	      Footer
	    </div> -->
	    <!-- /.box-footer-->
	</div>
  	<!-- /.box -->
</section>
<!-- /.content -->


@endsection

@section('script')

@endsection 