@extends('template.body')

@section('style')
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>In</li>
	    	<li class="active">Tambah</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
	      	<div class="box-body">
                <div class="form-group">
                  	<label class="col-md-2 control-label">Jenis / Slot</label>
                  	<div class="col-md-10">
        	          	<select class="form-control" data-placeholder="Pilih Slot" name="id_slot" required>
        	          	<option value="">Pilih Slot</option>
        	          	@if (!empty($slot))
	                  		@foreach($slot as $val)
	        					<option value="{{ $val['id_slot'] }}"> {{ $val['slot_name'] }}</option>
	        				@endforeach
        				@endif
        				</select>
                  </div>
                </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Panjang <small style="color: red">*</small> </label>
		          	<div class="col-md-10">
		          		<div class="input-group">
		          		      <input type="number" class="form-control" name="length" placeholder="Panjang kayu" required maxlength="100" value="{{ old('length') }}">
		          		      <span class="input-group-addon">CM</span>
		          		</div>
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Lebar <small style="color: red">*</small> </label>
		          	<div class="col-md-10">
		          		<div class="input-group">
		          		      <input type="number" class="form-control" name="width" placeholder="Lebar kayu" required maxlength="100" value="{{ old('width') }}">
		          		      <span class="input-group-addon">CM</span>
		          		</div>
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Jumlah <small style="color: red">*</small> </label> </label>
		          	<div class="col-md-10">
		            <input type="number" class="form-control" name="qty" placeholder="Jumlah" maxlength="100" value="{{ old('phone') }}">
		          	</div>
		        </div>
	      	</div>
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
				{{ csrf_field() }}	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();
	});
</script>
@endsection 