<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="@if (isset($menu) && $menu == 'home') active @endif">
          <a href="{{ url('home') }}">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <!-- --------------------------------------------------------------------- -->
        <!-- data master -->

        <li class="header">MASTER</li>
        <!-- admin & user -->
        <li class="treeview @if (isset($menu) && $menu == 'user') active @endif">
          <a href="javascript();">
            <i class="fa fa-user"></i>
            <span>User & Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'user tambah') active @endif"> <a href="{{ url('user/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'user list') active @endif"><a href="{{ url('user') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        <!-- slot -->
        <li class="treeview @if (isset($menu) && $menu == 'slot') active @endif">
          <a href="javascript();">
            <i class="fa fa-archive"></i>
            <span>Slot</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'slot tambah') active @endif"><a href="{{ url('slot/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'slot list') active @endif"><a href="{{ url('slot') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        <!-- kayu -->
        <!-- 
        <li class="treeview @if (isset($menu) && $menu == 'kayu') active @endif">
          <a href="javascript();">
            <i class="fa fa-industry"></i>
            <span>Kayu</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('kayu/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li><a href="{{ url('kayu') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
         -->
        <li class="header">Transaksi</li>
        <!-- IN -->
        <li class="treeview @if (isset($menu) && $menu == 'in') active @endif">
          <a href="javascript();">
            <i class="fa fa-angle-double-down"></i>
            <span>IN</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'in tambah') active @endif"><a href="{{ url('in/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'in list') active @endif"><a href="{{ url('in') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        <!-- out -->
        <li class="treeview  @if (isset($menu) && $menu == 'out') active @endif">
          <a href="javascript();">
            <i class="fa fa-angle-double-up"></i>
            <span>OUT</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'out tambah') active @endif"><a href="{{ url('out/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'out list') active @endif"><a href="{{ url('out') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>

        <li class="header">Report</li>
        <!-- IN -->

        <li class="@if (isset($menu) && $menu == 'report stok') active @endif">
          <a href="{{ url('report') }}">
            <i class="fa fa-book"></i> <span>Stok Kayu</span>
          </a>
        </li>
        <li class="@if (isset($menu) && $menu == 'report') active @endif">
          <a href="{{ url('report') }}">
            <i class="fa fa-bar-chart"></i> <span>Report</span>
          </a>
        </li>
        
                
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>